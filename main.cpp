#include<iostream>

using namespace std;
typedef unsigned char *pointer;

void show_bytes(pointer start,size_t len){
    size_t i;
    for ( i = 0; i < len; i++)
    {
        // %p \t 0x %.2x  \n
        // size_t 是sizeof操作符的结果类型
        printf("%p\t0x%.2x\n",start+i,start[i]);

    }
    printf("\n");
}
    

int main(){
    int a = 1024;
    show_bytes((pointer)&a,sizeof(int));
    return 0;
}

